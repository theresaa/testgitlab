
from crypt import methods
from flask import Flask,render_template,request
import requests,json
import psycopg2

hostname='localhost'
database='demo'
username='postgres'
pwd='TMPostgres'
port_id=5432
conn=None
cur=None
try:
    conn=psycopg2.connect(
            host=hostname,
            dbname=database,
            user=username,
            password=pwd,
            port=port_id
        
    )
    cur=conn.cursor()
except Exception as error:
    print(error)


from forms import InsertNameForm, InsertAgeForm

app=Flask(__name__)
app.config['SECRET_KEY']='theresax'

@app.route("/",methods=['GET','POST'])
@app.route("/names.html",methods=['POST','GET'])
def main():
    form=InsertNameForm()
    if (form.is_submitted() and form.username.data is not None):

        name=form.username.data
        def name_exist(name):
            exist=f'''SELECT *
            FROM names
            WHERE name='{name}';'''
            cur.execute(exist)
            conn.commit() 
            if(cur.fetchall()!=[]):
                return f"{name} already exists in db"
            else:
                return f"{name} doesn't exist in db"
        exists=name_exist(name)
        
        api_using_name=f"https://api.agify.io/?name={name}"
        response=requests.get(api_using_name)
    
        data=response.text
        parse_json=json.loads(data)
        #count=parse_json['count']
        age=parse_json['age']
        try:
            
            query=f'''SELECT name FROM names WHERE name='{name}';'''
            cur.execute(query)
            conn.commit()

            if(cur.fetchone() is not None):
                query=f'''SELECT count FROM names WHERE name='{name}';'''
                cur.execute(query)
                conn.commit()
                count=cur.fetchone()[0]+1
                query=f'''UPDATE names SET count={count} WHERE name='{name}';'''
                cur.execute(query)
                conn.commit()
                
            
            else:
                query='INSERT INTO names (name,age) VALUES (%s,%s);'
                insert_value=(name,age) 
                cur.execute(query,insert_value)
                conn.commit()
                
                
            message="YOUR RESULT:"
            nameresult=f"Name: {name}"
            ageresult=f"Age: {age}"

            select_name= """SELECT * FROM names"""
            cur.execute(select_name)
            result = cur.fetchall()
            print (result)

            cur.execute("SELECT SUM(age) FROM names")
            age_sum=cur.fetchone()[0]
            conn.commit()
            cur.execute('SELECT count(name) FROM names;')
            count=cur.fetchone()[0]
            conn.commit()
            avg=age_sum/count
            average=f"Average of the ages: {avg}"
            totalcount=f"Total results added : {count}"

            cur.execute("SELECT SUM(age) FROM names")
            age_sum=cur.fetchone()[0]
            conn.commit()
            cur.execute('SELECT count(name) FROM names;')
            count=cur.fetchone()[0]
            conn.commit()
            print(f"The average of the ages in the db is: {age_sum/count}")
            
            query=f'''SELECT count FROM names WHERE name='{name}';'''
            cur.execute(query)
            conn.commit()
            occurence=f'The total number of occurence of {name} is {cur.fetchone()[0]}'


        except Exception as error:
            print (error)

       

        return render_template('names.html',form=form,message=message,nameresult=nameresult,ageresult=ageresult,average=average,totalcount=totalcount,exists=exists,result=result,occurence=occurence)
    else:render_template('names.html',form=form)
    return render_template('names.html',form=form)

@app.route("/db.html")
def statisdatabasedatabastcs():
    select_name= """SELECT * FROM names"""
    cur.execute(select_name)
    result = cur.fetchall()
    return render_template("db.html",result=result)

@app.route("/statistics.html")
def statistics():
    cur.execute("SELECT SUM(age) FROM names")
    age_sum=cur.fetchone()[0]
    conn.commit()
    cur.execute('SELECT count(name) FROM names;')
    count=cur.fetchone()[0]
    conn.commit()
    avg=age_sum/count
    average=f"Average of the ages: {avg}"
    totalcount=f"Total results added : {count}"

    cur.execute("SELECT SUM(age) FROM names")
    age_sum=cur.fetchone()[0]
    conn.commit()
    cur.execute('SELECT count(name) FROM names;')
    count=cur.fetchone()[0]
    conn.commit()
    print(f"The average of the ages in the db is: {age_sum/count}")
    return render_template("statistics.html",average=average,totalcount=totalcount)
@app.route("/age.html",methods=['POST','GET'])
def Age():
    ageform=InsertAgeForm()    
    age=ageform.agefield.data
    if(ageform.is_submitted() and ageform.agefield.data is not None):   
        def find_closest_age():
            query=f'''SELECT age from names'''
            cur.execute(query)
            conn.commit() 
            ages=cur.fetchall()  
            array=[]
            for agee in ages:
                print(agee[0])
                array.append(agee[0])
                
            print(array)
            minimum=age

            for num in array:
                min=abs(age-num)
                if(min<minimum):
                    minimum=min
                    minage=num

            return minage
        query=f'''SELECT name from names WHERE age='{age}'; '''
        cur.execute(query)
        conn.commit()
        name=cur.fetchone()
        if(name is not None):
            exists=f"The age {age} exists in the db for the name {name[0]}"
        else:
            exists=f"No names in the db have an age of {age}"
            query=f'''SELECT name,age from names WHERE age='{find_closest_age()}';'''
            cur.execute(query)
            conn.commit()
            result=cur.fetchone()
            exists+=f"\n. The name with the closest age is {result[0]} with an age of {result[1]}"
     
        return render_template('age.html',ageform=ageform,exists=exists)
      
    return render_template('age.html',ageform=ageform)

if __name__=='__main__':
    app.run(port=5008)
#ATBBSCsJQGgBvjHk4pChDpNBMc8gDE57272F
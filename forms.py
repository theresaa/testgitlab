from flask_wtf import FlaskForm
from wtforms import StringField,SubmitField,IntegerField

class InsertNameForm(FlaskForm):
    username=StringField('Enter Name')
    submit=SubmitField('Submit')

    
class InsertAgeForm(FlaskForm):
    agefield=IntegerField('Enter Age')
    submitage=SubmitField('Submit')

